module Tezos.Nairobi.Unsafe (module X) where

import Tezos.Nairobi.ProtocolConstants as X (unsafeEstimatePastTimestamp)
